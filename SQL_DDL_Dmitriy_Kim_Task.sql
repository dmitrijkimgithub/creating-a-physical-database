CREATE SCHEMA store;

SET search_path TO store;

CREATE TABLE Customers (
    customer_id INT PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    full_name TEXT GENERATED ALWAYS AS (first_name || ' ' || last_name) STORED,
    email_address VARCHAR(100) UNIQUE NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    gender CHAR(1),
    date_of_birth DATE,
);

CREATE TABLE Products (
    product_id INT PRIMARY KEY,
    product_name VARCHAR(100) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    stock_quantity INT NOT NULL,
);

CREATE TABLE Orders (
    order_id INT PRIMARY KEY,
    customer_id INT NOT NULL,
    product_id INT NOT NULL,
    quantity INT NOT NULL,
    order_date DATE NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES Customers (customer_id),
    FOREIGN KEY (product_id) REFERENCES Products (product_id),
);

ALTER TABLE Customers
ADD CONSTRAINT DateOfBirth CHECK(date_of_birth > '2000-01-01');

ALTER TABLE Products
ADD CONSTRAINT Price CHECK(price >= 0);

ALTER TABLE Customers
ADD CONSTRAINT Gender CHECK(gender IN('M', 'F'));

INSERT INTO Customers (customer_id, first_name, last_name, email_address, phone_number, gender, date_of_birth)
VALUES (1, 'Joseph', 'Joy', 'joseph_joy@gmail.com', '4155551234', 'M', '2001-05-10'),
       (2, 'Jane', 'Smith', 'jane_smith@gmail.com', '2125556789', 'F', '2003-12-20'),
       (3, 'Alice', 'Johnson', 'alice_johnson@gmail.com', '7863554321', 'F', '2002-07-15'),
       (4, 'Michael', 'Nolan', 'michael_nolan@gmail.com', '6175549876', 'M', '2000-03-25'),
       (5, 'Emma', 'Thompson', 'emma_thompson@gmail.com', '9726754580', 'F', '2004-11-12');

INSERT INTO Products (product_id, product_name, price, stock_quantity)
VALUES (1, 'Laptop', 369.00, 20),
       (2, 'Smart Watch', 39.41, 30),
       (3, 'Glasses', 24.99, 10),
       (4, 'Ice Skates', 140.33, 24),
       (5, 'Portable Projector', 49.99, 10),
       (6, 'Jacket', 31.89, 14);

INSERT INTO Orders (order_id, customer_id, product_id, quantity, order_date)
VALUES (1, 1, 5, 2, '2022-01-05'),
       (2, 2, 2, 1, '2022-02-10'),
       (3, 1, 4, 3, '2022-12-15'),
       (4, 4, 1, 1, '2022-03-10'),
       (5, 3, 3, 2, '2022-09-05'),
       (6, 5, 2, 1, '2022-04-20'),
       (7, 2, 3, 1, '2022-02-11'),
       (8, 4, 2, 1, '2022-03-15'),
       (9, 3, 6, 1, '2022-08-28');
       
ALTER TABLE Customers ADD record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Products ADD record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Orders ADD record_ts DATE DEFAULT CURRENT_DATE;